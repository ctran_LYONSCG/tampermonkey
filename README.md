## Requirements:
You need tampermonkey extensions
## How to use:
Basiccall install the scripts using tampermonkey and turn it on. Done!
## What they do:
 + Reddit Post color alternator will alternate the background color for each post on reddit.
 It makes it easier to distinguish where the previous post ends and where the next post begins.
 + Gmail row highlighter will highlight what ever row in your inbox your mouse is hovering on. It eliminates 
 the need of finding where the mouse is when you want to click on an email.