// ==UserScript==
// @name         Gmail row highlighter
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://mail.google.com/mail/
// @grant        none
// @require http://code.jquery.com/jquery-latest.js
// ==/UserScript==

(function() {
    'use strict';
var trBackground;
    // Your code here...
    $(document).ready(function (){
        $('.zA, .yO').hover( function () {
            trBackground = $(this).css('background-color');
            $(this).css('background-color', 'grey');
        }, function() {
            $(this).css('background-color', trBackground);
        });
    });
})();