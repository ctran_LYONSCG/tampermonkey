// ==UserScript==
// @name         Reddit Post color alternator
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.reddit.com/
// @grant        none
// @require http://code.jquery.com/jquery-latest.js
// @require //code.jquery.com/ui/1.12.1/jquery-ui.js
// ==/UserScript==

(function() {
    'use strict';

    // Your code here...
    $(document).ready(function() {
        // color alternator
        $('.top-matter:odd').css('background-color','#EEE');
        $('.score, .unvoted').css('color','teal');

        // image zoom on hover
        $('img').hover(function(){
           // on mouse in
           $(this).show( "fold", 1000 );
        }, function(){
           // on mouse out
        });
    });
})();